# Setting up a remote office for working from home

I recently changed jobs, first day is 18th May 2020. This Project is to record Issues and detail steps I'm making to set up my office at home.

I'm doing this so that I can have a record, and also because it's a convinient way to re-aquaint myself with some of the features of GitLab (the product).

So, the project Files are realy only this README. See the [Issues Board](https://gitlab.com/sinewalker/gitlab-home-office/-/boards) (and maybe eventually the [Wiki](https://gitlab.com/sinewalker/gitlab-home-office/-/wikis/home)?) for more.

I'm also aware that I will probably have a separate GitLab account for my role in the company. So this Project will probably be forked or move under the other namespace, once it's ready.

---

## remote tips
- be proactive, get the work done
- schedule the day, think about which hours work for you
- self managed/lead
- mental set: rituals to start and end the day (breakfast, shower, dress)
- regular meditations part of the ritual
- background noise (mgt and also adding)
- expect to spend some time discovering your rythm, what you like, don't like
- look at it as an exciting new thing
- do hang out and have fun
- understand why you want to be remote
- it's about the work being done, not the hours you work
- be careful not to work the weekends or the evenings too much
- intentionality. do things, and make habits intentionaly
- intentionality: defining what a solid day’s work looks like
- set goals and objectives
- search for the answer first

## issues:
- working all the time? or appearing to be working all the time
- managing health (physical activity/exercise)
- taking breaks
- disciplin
- being with people (physically)
- making connections with people (virtually)
- distractions (same as focus - go to a public space where you're observed)


## skills:
- learning through trial/error
- which hours are best or worst
- need help focussing: change scenery, maybe somewhere where you're observed
- set boundaries around when you're working and when you're not ("guard rails") else you'll overwork

---

## Onboarding handbook links

 - [x] [Code of Conduct](https://about.gitlab.com/handbook/people-group/code-of-conduct/)

 - [x] [Guide for starting a remote job](https://about.gitlab.com/company/culture/all-remote/getting-started)
 - [x] [Our values](https://about.gitlab.com/handbook/values/)
 - [ ] [Communication](https://about.gitlab.com/handbook/communication/)
 - [x] [CEO](https://about.gitlab.com/handbook/ceo/)
 - [x] [Strategy](https://about.gitlab.com/company/trategy/)
 - [x] [Our product](https://about.gitlab.com/product/)
 - [x] [How to search GitLab like a pro](https://about.gitlab.com/handbook/tools-and-tips/searching/)
 - [ ] [Culture section of the GitLab blog](https://about.gitlab.com/blog/categories/culture/)

### Support Engineer Handbook

 - [Support Team Handbook](https://about.gitlab.com/handbook/support/)
   - [Support Engineer Responsibilities](https://about.gitlab.com/handbook/support/support-engineer-responsibilities.html)
 - [Support Workflows](https://about.gitlab.com/handbook/support/workflows/)
 - [Engineering Division](https://about.gitlab.com/handbook/engineering/)

---

## Mental Health

 - [We need to talk Mental Health](https://www.linkedin.com/video/live/urn:li:ugcPost:6666517232991911936/) by Natalie MacDonald, Together in Business [video](https://www.linkedin.com/video/live/urn:li:ugcPost:6666517232991911936/)
 - [Some guided meditations](https://mindfulcreation.com/whentimesaretough?s2-ssl=yes) from Reuben Lowe, [Mindful Creation](https://mindfulcreation.com)
